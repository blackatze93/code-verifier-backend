import { LogSuccess, LogError } from './src/utils/logger';
import dotenv from 'dotenv';
import server from './src/server';

// * Load environment variables
dotenv.config();

const port: string | number = process.env.PORT || 3000;

// * Start server
server.listen(port, () => {
    LogSuccess(`[SERVER ON]: Running in http://localhost:${port}/api`);
});

// * Server error handling
server.on('error', (error: any) => {
    LogError(`[SERVER ERROR]: ${error}`);
});