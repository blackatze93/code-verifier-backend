import express, { Express, Request, Response } from 'express';

// Security
import cors from 'cors';
import helmet from 'helmet';

// TODO: HTTPS

// Root Router
import rootRouter from '../routes';

// * Create Express APP
const server: Express = express();

// * Define SERVER to use "/api" and use rootRouter from 'index.ts' in routes
// From this point onover: http://localhost:8000/api/..
server.use('/api', rootRouter);

// * Security config
server.use(helmet());
server.use(cors());

// * Content Type config
server.use(express.urlencoded({ extended: true, limit: '50mb' }));
server.use(express.json({ limit: '50mb' }));

// * Redirects
server.get('/', (req: Request, res: Response) => {
    res.redirect('/api');
});

export default server;