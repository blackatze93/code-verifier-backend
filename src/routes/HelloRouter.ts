import express, { Request, Response } from "express";
import { HelloController } from "../controller/HelloController";
import { LogInfo } from "../utils/logger";

// Router from express
let helloRouter = express.Router();

// http://localhost:8000/api/hello?name=Martin/
helloRouter.route('/')
    // GET:
    .get(async (req: Request, res: Response) => {
        // Obtain the name from the query
        let name: any = req?.query?.name;
        LogInfo(`Query Param: ${name}`);

        // Create a new instance of the controller
        let helloController = new HelloController();

        // Call the controller method
        let response = await helloController.getMessage(name);

        // Send the response
        return res.send(response);
    });


export default helloRouter;