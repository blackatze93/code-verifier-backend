/**
 * Root Router
 * Redirections to Routers
 */

import { LogInfo } from '../utils/logger';
import express, { Request, Response } from 'express';
import helloRouter from './HelloRouter';

// Server instance
let server = express();

// Router instance
let rootRouter = express.Router();

// GET: http://localhost:8000/api/
rootRouter.get('/', (req: Request, res: Response) => {
    LogInfo('GET: http://localhost:8000/api/')
    res.send('Hola mundo gatuno!');
});


// Redirections to Routers & Controllers
server.use('/', rootRouter); // http://localhost:8000/api/
server.use('/hello', helloRouter); // http://localhost:8000/api/hello --> HelloRouter


export default server;